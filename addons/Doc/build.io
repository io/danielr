AddonBuilder clone do(
	dependsOnBinding("Socket")
	dependsOnBinding("CGI")
	dependsOnBinding("Regex")
)
