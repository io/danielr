Doc do(
	Response := Object clone do(
		appendProto(Doc)
		
		info := lazySlot(
			Map clone atPut("Content-type", "text/html")
		)

		statusStrings := Map clone do(
			atPut("200", "OK")
			atPut("404", "NOT FOUND")
			atPut("500", "INTERNAL SERVER ERROR")
		)

		statusCode ::= 200

		status := method(
			statusStrings at(statusCode asString)
		)


		content ::= ""

		atPut := method(name, value,
			info atPut(name, value)
			self
		)

		at := method(name,
			info at(name)
		)

		statusLine := method(
			"HTTP/1.0 #{statusCode} #{status}" interpolate			
		)

		infoLines := method(
			lines := info asList map(item,
				key := item at(0)
				value := item at(1)
				"#{key}: #{value}" interpolate
			)
		)

		headerLines := method(
			list(statusLine) union(infoLines with("", ""))			
		)

		header := method(
			headerLines join("\n")
		)

		asString := method(
			header .. content
		)
	)

	ErrorResponse := Response clone do(
		content := method(
			Templates at("#{statusCode}.html" interpolate) render(self)
		)
	)

	NotFoundResponse := ErrorResponse clone setStatusCode(404)

	ServerErrorResponse := ErrorResponse clone do(
		error ::= ""
		setStatusCode(500)
	)
)
