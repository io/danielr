Doc do(
	Data := Object clone do(
		appendProto(Doc)
		
		rootPath := lazySlot(
			Path with(AddonLoader addonFor("Doc") addonPath, "data")
		)

		dir ::= ""

		filePath := lazySlot(Path with(rootPath, dir)) 
	
		fileAt := method(path,
			file := File with(Path with(filePath, path))
			if(file exists, file, nil)		
		)
	)

	Media := Data clone do(
		setDir("media")

		contentsOf := method(path,		
			fileAt(path) ?contents
		)
	)

	Templates := Data clone do(
		setDir("templates")

		templates := Map clone

		at := method(path,
			template := templates at(path)
	
			file := fileAt(path)
			if(file isNil, return nil)

			if(template ?timeStamp < file lastDataChangeDate,
				template = nil
			)
			if(template isNil,
				template = Template clone setString(file contents) setTimeStamp(file lastDataChangeDate)
				templates atPut(path, template)
			)

			template
		)
	)
)
