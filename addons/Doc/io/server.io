Doc Server := Server clone do(
	appendProto(Doc)
	
	handleSocket := method(aSocket,
		RequestHandler clone @handleSocket(aSocket)
	)

	loadAddons := method(
		names := AddonLoader addons map(name)
		
		# Loading the Python addon does something weird that causes the
		# process not to be killable with ^C.
		# So for now, don't load it.
		names remove("Python")

		names foreach(name,
			try(
				doString(name)
			) catch(Exception,
				continue
			)
		)
		self
	)
	
	start := method(
		"Loading addons..." println
		loadAddons

		"Running documentation server on port #{port}..." interpolate println
		resend
	)
)
