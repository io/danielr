Regex

Doc Request := List clone do(
	appendProto(Doc)
	
	fromUrl := method(url,
		path := Request clone
		parts := url splitNoEmpties("/")
		path appendSeq(parts map(part, decodeUrlParam(part)))
	)
	
	asUrl := method(
		"/" .. map(part, encodeUrlParam(part)) join("/")
	)

	_dotRegex := "\\.+" asRegex
	_encodedDotsRegex := "([0-9])d" asRegex

	encodeUrlParam := method(aString,
		match := aString findRegex(_dotRegex)
		if(match,
			# Replace "." with "1d", ".." with "2d", etc.
			return "#{match string size}d" interpolate
		)
		CGI encodeUrlParam(aString)
	)

	decodeUrlParam := method(aString,
		match := aString findRegex(_encodedDotsRegex)
		if(match,
			# Replace "1d" with ".", "2d" with "..", etc.
			return "." repeated(match at(1) asNumber)
		)
		CGI decodeUrlParam(aString)
	)
)
