Regex

Doc Template := Object clone do(
	appendProto(Doc)
	
	timeStamp ::= nil
	codeRegex := """^[ \t]*%(.+)$""" asRegex multiline 
	renderMessage := lazySlot(Message clone)

	setString := method(string,
		matches := string matchesOfRegex(codeRegex)
		parts := list
		matches foreachInterval(match,
			s := match at(1) asMutable strip
			parts append(s)
			,
			s := match asMutable rstrip
			parts append("write(\"\"\"#{s}\"\"\" interpolate)" interpolate)
		)

		renderMessage = parts join("\n") asMessage
		self
	)

	render := method(context,
		if(context isKindOf(RenderContext) not,
			context = RenderContext clone appendProto(context)
		)
		renderMessage doInContext(context)
		context text
	)
)

Doc RenderContext := Object clone do(
	appendProto(Doc)

	text := lazySlot("")

	write := method(aString,
		text = text	.. aString
		self
	)

	extend := method(filename,
		context := RenderContext clone appendProto(self)
		context toBeFilledIn := text
		text = Templates at(filename) render(context)
		self
	)
)
