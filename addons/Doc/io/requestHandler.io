Doc Page := Object clone do(
	appendProto(Doc)
	
	columns ::= list

	render := method(
		Templates at("index.html") render(self)		
	)
)

Doc RequestHandler := Object clone do(
	appendProto(Doc)
	
	NotFoundError := Exception clone
	
	handleSocket := method(aSocket,
		aSocket streamReadNextChunk

		url := aSocket readBuffer betweenSeq("GET ", " HTTP")
		request := Request fromUrl(url)

		e := try(
			response := responseFor(request)
		)
		e catch(NotFoundError,
			response := NotFoundResponse clone
		) catch(Exception,
			response := ServerErrorResponse setError(e error) clone
		)
		
		aSocket streamWrite(response asString)
		aSocket close
	)
	
	responseFor := method(request,
		if(request first == "favicon.ico",
			raise404
		)

		if(request first == "media",
			content := Media contentsOf(request slice(1) join("/"))
			if(content isNil, raise404)
			return content
		)
		
		Response clone setContent(pageFor(request) render)
	)
	
	pageFor := method(request,
		columns := list(Column)
		request foreach(name,
			column := columns last selectSlot(name) atSelectedSlot
			if(column isNil,
				raise404
			)
			columns append(column)
		)
		Page clone setColumns(columns)
	)
	
	raise404 := method(
		NotFoundError raise
	)
)
