Doc Link := Object clone do(
	url ::= "#"
	name ::= ""

	class ::= nil
	
	classAttr := method(
		if(class, "class=\"#{class}\" " interpolate, "")
	)
	
	asString := method(
		"<a #{classAttr}href=\"#{url}\">#{name}</a>" interpolate
	)
)
