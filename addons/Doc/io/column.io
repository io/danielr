Doc Column := Object clone do(
	appendProto(Doc)
	
	path := lazySlot(Request clone)

	_object := Lobby Protos

	object := method(
		self getSlot("_object")
	)

	isProto := true

	isMethod := method(
		isProto not	
	)
	
	atSlot := method(name,
		if(object hasSlot(name) not,
			return nil
		)

		o := object getSlot(name)

		col := Column clone
		col _object := getSlot("o")
		col path := path with(name)
		col isProto := name at(0) isUppercase
		col selectedSlot := nil

		if(col isMethod,
			col info := infoForSlot(name)
		)

		col
	)
	

	selectedSlot := nil

	selectSlot := method(name,
		selectedSlot = name
		self
	)
	
	atSelectedSlot := method(
		atSlot(selectedSlot)
	)
	

	info := lazySlot(mapForDocs(object docs))
	
	infoForSlot := method(name,
		slotInfo := info at("slots")
		if(slotInfo isNil, return nil)
		
		if(slotInfo slotNames contains(name),
			return mapForDocs(slotInfo getSlot(name))
		)
		nil
	)
	
	mapForDocs := method(docs,
		docMap := Map clone do
		docs slotNames foreach(name,
			docMap atPut(name, docs getSlot(name))
		)
		docMap
	)


	url := method(
		path asUrl .. "/"
	)
	
	link := method(
		name := path last ifNilEval("Protos")

		link := Link clone setUrl(url) setName(name)
		if(isMethod and info isNil,
			link setClass("undocumented")
		)
		link
	)
	
	slotLinks := method(
		names := object slotNames remove("")
		names sort map(name,
			link := atSlot(name) link
			if(name == selectedSlot,
				link setClass("active")
			)
			link
		)
	)
	

	template := method(
		if(isProto,
			Templates at("protoColumn.html")
			,
			Templates at("methodColumn.html")
		)
	)

	render := method(
		template render(self)
	)
)
