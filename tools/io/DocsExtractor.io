DocsExtractor := Object clone do(
	init := method(
		self folder := Directory clone
		self outFile := File clone
	)
	
	setPath := method(path,
		folder setPath(Path with(path, "source"))
		l := launchPath asMutable clipAfterStartOfSeq("/../../")
		try(Lobby doString(l lastPathComponent))
		outFile setPath(Path with(path, "io/zzz_docs.io"))
		self
	)
	
	clean := method(
		outFile remove
		self
	)

	extract := method(
		outFile remove open
		sourceFiles foreach(file,
			"Extracting io comment code from #{file name}" interpolate println
			slices := file contents slicesBetween("/*#io", "*/") map(strip)
			if(slices isEmpty, continue)

			code := slices join("\n") .. "\n)\n\n"
			e := try(
				Lobby doString(code)
			)
			e catch(Exception,
				"Error in docs strings of #{file name}:\n  #{e error}" interpolate println
				continue
			)
			if(e isNil,
				outFile write(code)
			)
		)
		self
	)

	sourceFiles := method(
		folder files select(file,
			file name beginsWithSeq("Io") and
			file name containsSeq("Init") not and
			file name pathExtension == "c"
		)
	)
)

de := DocsExtractor clone
de setPath(args at(1))
if(args at(3) == "clean", de clean, de extract)
